#!/usr/bin/env python3
import os
import sys
import gi

gi.require_version('Gtk', '3.0')
from gi.repository import Gtk

class MyWindow(Gtk.ApplicationWindow):

    def __init__(self, app):
        Gtk.Window.__init__(self, title="Youtube downloader V0.1", application=app)
        self.set_default_size(100, 50)
        self.set_position(Gtk.WindowPosition.CENTER)
        self.set_border_width(15)

        grid = Gtk.Grid()
        grid.set_column_spacing(10)
        grid.set_row_spacing(4)
        self.add(grid)

        lable_ydl = Gtk.Label(label="Youtube video URL", halign=Gtk.Align.START)

        self.entry = Gtk.Entry()
        self.entry.set_width_chars(50)
        self.entry.set_placeholder_text("URL")

        button_icon_install = Gtk.Button.new_with_label("Download")
        button_icon_install.set_size_request(90, 40)
        button_icon_install.connect("clicked", self.download_video)

        switch = Gtk.Switch()
        switch.connect("notify::active", self.download_video)
        switch.set_active(False)

        grid.attach(lable_ydl, 0, 0, 1, 1)
        grid.attach(self.entry, 0, 1, 1, 1)
        grid.attach(button_icon_install, 2, 1, 1, 1)

    # ---Function to install themes
    def download_video(self, button):
        dl_video = "xterm -geometry 100x25 -e youtube-dl -f bestvideo[ext=mp4]+bestaudio[ext=m4a]/mp4 " + self.entry.get_text()
        os.system(dl_video)
        download_video_dialog = Gtk.MessageDialog(parent=self, modal=True, message_type=Gtk.MessageType.INFO,
                                                 buttons=Gtk.ButtonsType.OK, text="Video downloaded")
        download_video_dialog.run()
        download_video_dialog.destroy()


class MyApplication(Gtk.Application):

    def __init__(self):
        Gtk.Application.__init__(self)

    def do_activate(self):
        win = MyWindow(self)
        win.show_all()

    def do_startup(self):
        Gtk.Application.do_startup(self)


app = MyApplication()
exit_status = app.run(sys.argv)
sys.exit(exit_status)
